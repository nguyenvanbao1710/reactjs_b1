import "./App.css";
import Footer from "./components/Footer";
import Header from "./components/Header";
import Item from "./components/Item";
import Navbar from "./components/Navbar";

function App() {
  return (
    <>
      <Navbar />
      <Header />
      <div className="container">
        <Item />
      </div>
      <Footer />
    </>
  );
}

export default App;
